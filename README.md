# Horizen (Zencash) Nodes

Horizen Nodes help to establish one of the most secure, distributed, and resilient networks,
powering the Horizen ecosystem and offering enhanced privacy as the first to market end-to-end encrypted blockchain network.

This repository builds the images for deploying on
[Cryptonetes](https://cryptonetes.com/) - a cloud hosted service powered by Kubernetes:

We use the following user_data for deploying the required environment variables:

```
NODE_ID=$NODEID
NODE_TYPE=$NODETYPE
STAKEADDR=$STAKEADDR
FQDN=$FQDN
REGION=na
EMAIL=$EMAIL
IPV4ADDR=
```

## Donations

Donations are appreciated:

**ETH** 0xb08c615eb6e0269dc27ece4a581f87d2f5b7188d

**ZEN** znoYaPU2G7MzUA69cMSYzmqMG4rkw3Mx7ae
