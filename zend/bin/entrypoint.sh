#!/bin/bash
set -e

# Ensure directories exist
mkdir -p /mnt/zen/{config,zcash-params,certs}

# If volumes for zen or zcash-params are present, symlink them to user's home, if not create folders.
if [ ! -L /home/user/.zen ]; then
    ln -s /mnt/zen/config /home/user/.zen > /dev/null 2>&1 || true
fi

# zcash-params can be symlinked in from an external volume
if [ ! -L /home/user/.zcash-params ]; then
    ln -s /mnt/zen/zcash-params /home/user/.zcash-params > /dev/null 2>&1 || true
fi

# Generate a random rpc password (this port isn't even open)
rpcpassword=$(head -c 32 /dev/urandom | base64)

# Create zen.conf
cat <<EOF > /mnt/zen/config/zen.conf
rpcport=18231
rpcallowip=127.0.0.1
rpcworkqueue=512
server=1
# Docker doesn't run as daemon
daemon=0
listen=1
txindex=1
logtimestamps=1
rpcuser=user
rpcpassword=$rpcpassword
port=9033

# Temporarily disable certs
#tlscertpath=/mnt/zen/certs/cert.pem
#tlskeypath=/mnt/zen/certs/key.pem
EOF

echo "Trying to determine public IP address."
while [ -z "$publicips" ]; do
   publicips=$(dig $FQDN A $FQDN AAAA +short)
   echo -n "."
   sleep 5
done

while read -r line; do
    echo "externalip=$line" >> /mnt/zen/config/zen.conf
done <<< "$publicips"

echo "Copying additional trusted SSL certificates"
cp /mnt/zen/certs/ca.cer /usr/local/share/ca-certificates/ca.crt > /dev/null 2>&1 || true
update-ca-certificates --fresh

echo "Installing secnodetracker"
cd /home/user
git clone https://github.com/ZencashOfficial/secnodetracker.git
cd secnodetracker
npm install

if [ $NODE_TYPE == "super" ]; then servers=xns; else servers=ts; fi
mkdir -p /home/user/secnodetracker/config
cat << EOF > /home/user/secnodetracker/config/config.json
{
   "active":"$NODE_TYPE",
   "$NODE_TYPE":{
      "nodetype":"$NODE_TYPE",
      "nodeid":null,
      "servers":[
         "${servers}2.eu",
         "${servers}1.eu",
         "${servers}3.eu",
         "${servers}4.eu",
         "${servers}4.na",
         "${servers}3.na",
         "${servers}2.na",
         "${servers}1.na"
      ],
      "stakeaddr":"$STAKEADDR",
      "email":"$EMAIL",
      "fqdn":"$FQDN",
      "ipv":"4",
      "region":"$REGION",
      "home":"ts1.$REGION",
      "category":"none"
   }
}
EOF

# Fetch latest zen-fetch-params
zen-fetch-params

# Fix ownership of the created files/folders
chown -R 1001:0 /home/user /mnt/zen

# Start supervisord
/usr/bin/supervisord
