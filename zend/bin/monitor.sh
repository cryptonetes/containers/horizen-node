#!/bin/bash

### Set initial time of file
LTIME=$(stat -c %Z /mnt/zen/certs/cert.pem)

while true
do
    # Create a new address if it doesn't exist
    if [[ $(zen-cli z_listaddresses | wc -l) -eq 2 ]]; then
      echo "Creating a new address:"
      zen-cli z_getnewaddress
    fi

    block=$(zen-cli getinfo | grep \"blocks\" | tr -d \",)
    address=$(zen-cli z_listaddresses | tr -d []\"' ')
    balance=$(zen-cli z_gettotalbalance | tr -d {}\",)

    echo "============================="
    echo "Address: $address"
    echo "$block"
    echo "Balance: $balance"
    echo "============================="

    ATIME=$(stat -c %Z /mnt/zen/certs/cert.pem)
    if [[ "$ATIME" != "$LTIME" ]]
    then
       # Restart zend when SSL cert updated
       supervisorctl restart zend
       supervisorctl restart monitor
       LTIME=$ATIME
    fi

    sleep 600
done
